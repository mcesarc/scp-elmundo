#scp-elmundo

#LÉEME 

## Ámbito El Mundo   ##

Ámbito (scope) de la portada del diario El Mundo para Ubuntu Touch.

Se precisa la herramienta «scopecreator» para construirlo.


# README 

##  El Mundo Scope  ##

Scope for Ubuntu Touch.

Requires scopecreator tool to build.